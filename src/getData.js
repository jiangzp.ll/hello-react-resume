// eslint-disable-next-line no-unused-vars
import Person from './Person';
// eslint-disable-next-line no-undef,no-unused-vars
const $ = require('jquery');

const url = 'http://localhost:3000/person';

function fetchData(url) {
  return fetch(url)
    .then(response => response.json())
    .catch(error => error);
}

fetchData(url).then(response => {
  // eslint-disable-next-line no-console
  console.log(response);
});
